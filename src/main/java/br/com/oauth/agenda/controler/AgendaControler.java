package br.com.oauth.agenda.controler;

import br.com.oauth.agenda.model.Agenda;
import br.com.oauth.agenda.model.Usuario;
import br.com.oauth.agenda.model.dto.AgendaRequestDTO;
import br.com.oauth.agenda.model.mapper.AgendaMapper;
import br.com.oauth.agenda.service.AgendaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class AgendaControler {

    @Autowired
    AgendaService agendaService;

    @PostMapping("/contato")
    public Agenda cadastrarContato(@RequestBody AgendaRequestDTO agendaRequestDTO, @AuthenticationPrincipal Usuario usuario){

        Agenda agenda = AgendaMapper.fromAgendaRequestDTO(agendaRequestDTO, usuario);
        return agendaService.incluirContato(agenda);
    }

    @GetMapping("/contatos")
    public List<Agenda> buscarContatos(@AuthenticationPrincipal Usuario usuario){

        List<Agenda> contatosAgenda = new ArrayList<>();
        Iterable<Agenda> agendaIterable= agendaService.buscarClientePorUsuario(usuario.getName());
        for (Agenda agenda: agendaIterable ){
            contatosAgenda.add(agenda);
        }
        return  contatosAgenda;
    }
}

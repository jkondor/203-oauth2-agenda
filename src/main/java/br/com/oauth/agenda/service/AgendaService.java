package br.com.oauth.agenda.service;

import br.com.oauth.agenda.model.Agenda;
import br.com.oauth.agenda.repositories.AgendaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AgendaService {

    @Autowired
    AgendaRepository agendaRepository;

    public Agenda incluirContato(Agenda agenda){
        return agendaRepository.save(agenda);
    }

    public Iterable<Agenda> buscarClientePorUsuario(String usuario){
        return agendaRepository.findByUsuario(usuario);
    }
}

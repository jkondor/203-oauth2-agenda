package br.com.oauth.agenda.model.dto;

public class AgendaRequestDTO {
    private String contato;
    private String telefone;

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}


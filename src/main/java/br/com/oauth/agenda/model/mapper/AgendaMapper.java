package br.com.oauth.agenda.model.mapper;

import br.com.oauth.agenda.model.Agenda;
import br.com.oauth.agenda.model.Usuario;
import br.com.oauth.agenda.model.dto.AgendaRequestDTO;

public class AgendaMapper {

    public static Agenda fromAgendaRequestDTO(AgendaRequestDTO agendaRequestDTO, Usuario usuario){
        Agenda agenda= new Agenda();

        agenda.setUsuario(usuario.getName());
        agenda.setContato(agendaRequestDTO.getContato());
        agenda.setTelefone(agendaRequestDTO.getTelefone());

        return agenda;
        }
}

package br.com.oauth.agenda.repositories;

import br.com.oauth.agenda.model.Agenda;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AgendaRepository extends CrudRepository<Agenda, Integer> {

    Iterable<Agenda> findByUsuario(String usuario);
}
